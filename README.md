﻿# Coolshop backend test

## Introduction

This is a simple backend test for the position of junior software developer at Coolshop.

For an overview of the solution, please see the [solution description](#solution).

## Task

The task is to create a console application that can take 3 arguments:

1. The path to a CSV file formatted as described below
    ```csv
    1,Rossi,Fabio,01/06/1990;
    2,Gialli,Alessandro,02/07/1989;
    3,Verdi,Alberto,03/08/1987;
    ```
2. A number representing the index of the column
3. A string representing the value to search for

The application should then search for the value in the column and print the row to the console.

## Example

```bash
$ ./CsvSearch ./data.csv 1 Gialli
2,Gialli,Alessandro,02/07/1989;
```

## Requirements

- The due date is 2023-01-30.
- The application should be written in C#.
- The application should be a console application.
- The application should be hosted on a public repository.

## Evaluation

The evaluation will be based on the following criteria:

- The code is clean and easy to read.
- The code is well structured.
- Relevance of the solution to the requirements.

## Solution

There are multiple solutions to this problem, from the most simple to the most complex. I decided to explore 3 of them, each with their own pros and cons. To see the code for each solution, please see the corresponding branch.

In addition to the 3 solutions, I also decided to implement a CI/CD pipeline. It's a very simple pipeline that builds the application and packages it as binary for Windows, Linux and MacOS. Furthermore, when a tag is pushed to the repository, a release is created with the binaries attached.

### Solution 1: Simple

This solution (released with tag [v1.0.3](https://gitlab.com/DaruZero/coolshop-backend-test/-/releases/v1.0.3)), is the "quick and dirty" way of solving the problem.

Is simple and easy to understand: it verifies the arguments, reads the file line by line, and prints the line if the value is found.

The only checks it does are:

- The number of arguments is correct
- The file exists
- The column index is a valid integer

It uses an external library to parse the CSV file, [Sylvan.Data.Csv](https://www.nuget.org/packages/Sylvan.Data.Csv), that, according to multiple sources ([here](https://www.joelverhagen.com/blog/2020/12/fastest-net-csv-parsers) and [here](https://github.com/MarkPflug/Benchmarks/blob/main/docs/CsvReaderBenchmarks.md)), is the fastest library for parsing CSV files.

The drawback of this solution is that it is not very extensible. If we want to change the CSV library, we would have to make big changes to the code.

The usage is simple and reflects the requirements. Download the binary for your OS, and run it from the command line.

```powershell
PS> .\CsvSearch.exe .\data.csv 1 Gialli
2,Gialli,Alessandro,02/07/1989;
```

### Solution 2: Intermediate

This solution (released with tag [v2.0.1](https://gitlab.com/DaruZero/coolshop-backend-test/-/releases/v2.0.1)) isn't necessarily better than the first from a performance standpoint, but it serves as a showcase on how to use classes to make the code more maintainable and reusable. There are a lot of possible concepts and patterns, like Dependency Injection or Method Factory, but this test is too small to fully implement those. 

For those reason I decided to make two changes:
- Move the validation of the arguments to a static class `Utils`, which could be expanded in the future with more utilities.
- Move all the logic related to the CSV file to the class `CsvReader`. That way the actual implementation of the methods it's hidden to the main Program, and only the interface is exposed. So in the future it could be possible to change library or even write a parser from scratch, without changing the interface, therefore leaving the main program untouched.

The drawback is that for a problem this small, this solution is almost overkill, and could be a little harder to read. This type of patterns are much better suited for larger projects.

The usage is the same as Solution 1.

```powershell
PS> .\CsvSearch.exe .\data.csv 1 Gialli
2,Gialli,Alessandro,02/07/1989;
```

### Solution 3: Advanced

This solution (released with tag [v3.0.0](https://gitlab.com/DaruZero/coolshop-backend-test/-/releases/v3.0.0)) is not necessarily more complex than the previous one, but it's a little more "advanced" in the sense that it's no longer a simple script, but a full CLI application.

Using flags to specify the arguments, it's possible to make the application more user friendly. The help is automatically generated, and the arguments can be specified in any order. Additionally, from a developer standpoint, it's easier to extend functionality and add new features.

The drawback is that it could be considered out of the requirements, since the usage is a bit different than the one specified in the task. However, I think that the benefits of using a CLI application outweigh the drawbacks.

The usage is the same as Solution 1, but with the flags.

```powershell
PS> .\CsvSearch.exe -f .\data.csv -i 1 -k Gialli
2,Gialli,Alessandro,02/07/1989;
```

## Conclusion

I think that the best solution is the one that best fits the requirements. In this case, the requirements are very simple, so the simplest solution is the best. However, if the requirements were to change, or if the application was to be extended, the more complex solutions would be better suited.

I also thought that it would be interesting to show the different ways of solving the problem, and how they could be used in different scenarios.

I considered adding a test project, but I decided against it, since the application is very simple and the test would be very simple as well. However, if the application was to be extended, it would be a good idea to add tests.
