﻿namespace CsvSearch;

public static class Utils
{
    /// <summary>
    /// Search for a key in a CSV file
    /// </summary>
    /// <param name="file">The path to a valid csv file.</param>
    /// <param name="index">The zero-based index of the column to search into.</param>
    /// <param name="key">The value to search.</param>
    public static void SearchValue(FileSystemInfo file, int index, string key)
    {
        // Check if the file exists
        if (!file.Exists)
            ExitWithError($"The file '{file.FullName}' does not exist.");

        // Check if the file is a CSV file
        if (file.Extension != ".csv")
            ExitWithError($"The file '{file.FullName}' is not a CSV file.");

        // Check if the column index is valid
        if (index < 0)
            ExitWithError("The column index cannot be negative.");

        // Check if the search key is valid
        if (string.IsNullOrWhiteSpace(key))
            ExitWithError("The search key cannot be empty.");

        try
        {
            // Read the CSV file
            using var reader = new CsvReader(file.FullName);

            while (reader.Read())
            {
                // Check if the current row contains the search key in the specified column
                if (reader.GetFieldString(index) != key) continue;

                // If the search key is found, print the row
                Console.WriteLine($"Match found at line : {reader.GetRowNumber()}\n{reader.GetRowString()}");

                reader.Dispose(); // not really needed, but it's good practice
                return;
            }

            Console.WriteLine($"No matches found for the key '{key}' in the column {index}");

            reader.Dispose(); // not really needed, but it's good practice
        }
        catch (ArgumentOutOfRangeException)
        {
            ExitWithError("The column index is out of range.");
        }
    }

    /// <summary>
    /// Exit the application with an error message.
    /// </summary>
    /// <param name="message">The message to output.</param>
    private static void ExitWithError(string message)
    {
        Console.WriteLine(message);
        Environment.Exit(1);
    }
}