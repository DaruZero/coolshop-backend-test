﻿// Author:      Matteo Danelon (matteo@danelon.me)
// Date:        2023-01-23
// Title:       CsvSearch
// Description: Create a console application that takes as input a csv file, a column index and a search key,
//              and returns the first row that matches the search key in the specified column.

using System.CommandLine;
using CsvSearch;

// Define the command line option for the file path
var fileOption = new Option<FileInfo>(
    aliases: new[] {"--file", "-f"},
    description: "Path to a valid CSV file.")
{
    IsRequired = true
};

// Define the command line option for the column index
var indexOption = new Option<int>(
    aliases: new[] {"--index", "-i"},
    description: "The zero-based index of the column to search into.")
{
    IsRequired = true
};

// Define the command line option for the search key
var keyOption = new Option<string>(
    aliases: new[] {"--key", "-k"},
    description: "The value to search.")
{
    IsRequired = true
};

// Define the root command
var rootCommand = new RootCommand("Search for a key in a CSV file.")
{
    fileOption,
    indexOption,
    keyOption
};

// Set the handler for the root command
rootCommand.SetHandler(Utils.SearchValue, fileOption, indexOption, keyOption);

// Run the command line application
return await rootCommand.InvokeAsync(args);