﻿namespace CsvSearch;

public interface ICsvReader : IDisposable
{
    bool Read();
    string GetFieldString(int index);
    int GetRowNumber();
    string GetRowString();

}