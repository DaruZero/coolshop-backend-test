﻿using System.Text;
using Sylvan.Data.Csv;

namespace CsvSearch;

internal sealed class CsvReader : ICsvReader
{
    private readonly CsvDataReader _reader;

    public CsvReader(string path)
    {
        _reader = CsvDataReader.Create(path, new CsvDataReaderOptions(){ HasHeaders = false });
    }

    public void Dispose()
    {
        if (_reader.IsClosed)
            return;
        _reader.Dispose();
    }

    public bool Read()
    {
        return _reader.Read();
    }

    public string GetFieldString(int index)
    {
        return _reader.GetString(index);
    }

    public int GetRowNumber()
    {
        return _reader.RowNumber;
    }

    public string GetRowString()
    {
        var sb = new StringBuilder();
        for (var i = 0; i < _reader.RowFieldCount; i++)
        {
            sb.Append(_reader.GetString(i));
            if (i + 1 != _reader.RowFieldCount)
                sb.Append(',');
        }
        return sb.ToString();
    }
}